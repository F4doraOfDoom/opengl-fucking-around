#pragma once
#include <array>
#include "index_buffer.hpp"
#include "vertex_buffer.hpp"
#include "buffer_opt.hpp"

namespace j2d
{
	namespace shapes
	{
		enum class shape_type
		{
			TRIANGLE = GL_TRIANGLES,
			SQUARE = GL_TRIANGLES
		};

		/// <summary>
		/// General class that describes all shapes
		/// </summary>
		class basic_shape
		{
		public:
			virtual void draw() const = 0;
		};


		/// <summary>
		/// General class that describes all shapes that have vertecies
		/// </summary>
		template <unsigned int N_Indices>
		class pointful_shape : public basic_shape
		{
		protected:
			// These buffers should be handeled by the inheriting shapes
			j2d::index_buffer<N_Indices>* i_buffer = nullptr;
			j2d::vertex_buffer<float>* v_buffer = nullptr;
		};

		class rectangle : public pointful_shape<6>
		{
		public:
			rectangle(const std::initializer_list<float>&& points)
			{
				options.shape_draw = j2d::buffer_opt::Shape::TRIANGLE;
				options.buffer_usage = j2d::buffer_opt::Usage::STATIC;

				v_buffer = new j2d::vertex_buffer<float>(points, options);
				i_buffer = new j2d::index_buffer<n_indices>(indices_data, options);
			}

			// TODO
			rectangle(rectangle&) = delete;
			void operator=(rectangle&) = delete;

			~rectangle()
			{
				delete v_buffer;
				delete i_buffer;
			}

			virtual void draw() const override
			{
				i_buffer->use_index_buffer();
			}

		private:
			j2d::buffer_opt options;
			static constexpr unsigned int n_indices = 6;
			static constexpr std::array<unsigned int, n_indices> indices_data {
				0, 1, 2,
				2, 3, 0
			};
		};
	}
}