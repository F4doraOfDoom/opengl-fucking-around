#pragma once

#define INDEX_BUFFER_MUST_BE_CONST 

#define STATIC_ASSERT( condition, name )\
    typedef char assert_failed_ ## name [ (condition) ? 1 : -1 ];