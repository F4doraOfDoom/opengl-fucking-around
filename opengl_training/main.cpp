#include <iostream>
#include <string>
#include <cstdio>
#include <ctime>
#include <sstream>

#define GLEW_STATIC
#include "GL/glew.h"
#include "GLFW/glfw3.h"

#include "vertex_buffer.hpp"
#include "index_buffer.hpp"
#include "buffer_opt.hpp"
#include "exceptions.hpp"
#include "shader.hpp"
#include "math.hpp"
#include "basic_shapes.hpp"

j2d::vertex_buffer<float>* v_buffer;
j2d::index_buffer<6> * i_buffer;
j2d::vertex_shader v_shader("");
j2d::fragment_shader f_shader("");

inline float gen_color()
{
	return static_cast<float>(rand()) / static_cast <float> (RAND_MAX);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	static double mousex, mousey;

	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		int swidth, sheight;
		glfwGetWindowSize(window, &swidth, &sheight);

		glfwGetCursorPos(window, &mousex, &mousey);

		mousex = j2d::math::map(mousex, 0.0, (double)sheight);
		mousey = j2d::math::map(mousey, 0.0, (double)sheight);

		v_buffer->add({ static_cast<float>(mousex), static_cast<float>(-mousey) });
		std::printf("Added at %f, %f\n", mousex, -mousey);
	}
}

void mouse_move_callback(GLFWwindow* window, double xpos, double ypos)
{
		std::stringstream code;
		code << "#version 330 core\n"
			"layout(location = 0) out vec4 color;\n"
			"void main()\n"
			"{\n"
			"	color = vec4(" << gen_color() <<  ", " << gen_color() << "," << gen_color() << "," << 1.0 <<");\n"
			"}\n";
		

		f_shader.recompile(code.str()).use();
}

int main()
{
	GLFWwindow* window{};
	srand((unsigned int)std::time(NULL));

	if (!glfwInit()) return -1;

	window = glfwCreateWindow(450, 450, "OpenGL", nullptr, nullptr);
	if (!window)
	{
		glfwTerminate();
		return -1;
	}

	glfwMakeContextCurrent(window);
	if (glewInit() != GLEW_OK)
		std::cout << "ERROR" << std::endl;

	j2d::shapes::rectangle rect{
		0.0f, 0.0f,
		0.0f, 0.5f,
		-0.5f, 0.5f,
		0.5f, 0.0f
	};

	glfwSetMouseButtonCallback(window, mouse_button_callback);
	glfwSetCursorPosCallback(window, mouse_move_callback);

	f_shader.initialize();
	v_shader.initialize();

	f_shader.recompile("#version 330 core\n"
		"layout(location = 0) out vec4 color;\n"
		"void main()\n"
		"{\n"
		"	color = vec4(1.0, 0.0, 0.0, 1.0);\n"
		"}\n");

	v_shader.recompile("#version 330 core\n"
		"layout(location = 0) in vec4 color;\n"
		"void main()\n"
		"{\n"
		"	gl_Position = position;\n"
		"}\n");

	f_shader.use();
	v_shader.use();

	while (!glfwWindowShouldClose(window))
	{
		glClear(GL_COLOR_BUFFER_BIT);

		rect.draw();
		//i_buffer->use_index_buffer();

		glfwSwapBuffers(window);
		glfwPollEvents();

	}

	glfwTerminate();
	delete v_buffer;
	delete i_buffer;
}

