#pragma once 

namespace j2d
{
	namespace math
	{
		template <typename T>
		T map(T input, T input_start = 1.0, T input_end = 100.0, T output_start = -1.0, T output_end = 1.0)
		{
			return output_start + ((output_end - output_start) / (input_end - input_start)) * (input - input_start);
		}
	}
}
