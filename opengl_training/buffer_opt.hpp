#pragma once
namespace j2d
{
	/// <summary>
	/// This struct represents the options for a BufferObject type
	/// </summary>
	struct buffer_opt
	{
		enum class Type : int
		{
			ARRAY = GL_ARRAY_BUFFER // The buffer represents an array
		};

		enum class Usage : int
		{
			FREQUENT = GL_DYNAMIC_DRAW, // The buffer will be changed frequently
			STATIC = GL_STATIC_DRAW // The buffer will not change, or very infrenquently
		};

		enum class Shape : int
		{
			LINE = GL_LINE, // The buffer represents one line
			TRIANGLE = GL_TRIANGLES, // The buffer represents a triangle, or multiple triangles 
			SQUARE = GL_TRIANGLES,
		};

		enum class Dimentions : int
		{
			TWO = 2,
			THREE = 3
		};

		inline int type() const { return static_cast<int>(buffer_type); }
		inline int usage() const { return static_cast<int>(buffer_usage); }
		inline int shape() const { return static_cast<int>(shape_draw); }
		inline int dimentions() const { return static_cast<int>(point_d); }

		Type buffer_type = Type::ARRAY;
		Usage buffer_usage = Usage::FREQUENT;
		Shape shape_draw = Shape::LINE;
		Dimentions point_d = Dimentions::TWO;
	};
}