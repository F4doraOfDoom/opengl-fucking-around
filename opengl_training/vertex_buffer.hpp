#pragma once

#include <initializer_list>
#include <algorithm>
#include <vector>
#include <array>

#include "GL/glew.h"
#include "buffer_opt.hpp"

namespace j2d
{
	/// <summary>
	/// This class represents an OpenGL buffer.
	/// It handles the appropriate OpenGL calls, and deals with the abstraction.
	/// Can be initialized with an initializer list of points, 
	/// and customized using BufferOpt
	/// </summary>
	template <typename BufferObjType>
	class vertex_buffer
	{
	public:
		/// <summary>
		/// Initialize the buffer using a list of points
		/// </summary>
		/// <param name="point_list"></param>
		/// <param name="options"></param>
		vertex_buffer(
			const std::initializer_list<BufferObjType>&& point_list,
			const buffer_opt& options = buffer_opt()) : vertex_buffer(point_list, options) { }

		/// <summary>
		/// Initialize the object with a vector of points
		/// </summary>
		/// <param name="point_list"></param>
		/// <param name="options"></param>
		vertex_buffer(
			const std::vector<BufferObjType>& point_list,
			const buffer_opt& options = buffer_opt()) : data(point_list), options(options)
		{
			create_gl_buffer();
		}

		void add(std::initializer_list<BufferObjType> new_points)
		{
			data.reserve(new_points.size());
			for (auto& p : new_points)
			{
				data.push_back(p);
			}

			copy_data_to_buffer();
			enable_vertex_buffer();
		}

		//TODO 
		vertex_buffer(vertex_buffer&) = delete;
		void operator=(const vertex_buffer&) = delete;

		/// <summary>
		/// Draw the object
		/// </summary>
		void draw_buffer()
		{
			glBindBuffer(options.type(), id);
			glDrawArrays(options.shape(), 0, data.size() / options.dimentions());
		}

		void bind()
		{

		}

		~vertex_buffer()
		{
			glDeleteBuffers(1, &id);
		}

	private:
		/// <summary>
		/// initializes the OpenGL buffer
		/// </summary>
		void copy_data_to_buffer()
		{
			if (data.size() > 0)
			{
				glBindBuffer(options.type(), id);
				glBufferData(options.type(), data.size() * object_size, &data.at(0), options.usage());
			}
		}

		void enable_vertex_buffer()
		{
			glEnableVertexAttribArray(0);
			glVertexAttribPointer(0, options.dimentions(), GL_FLOAT, GL_FALSE, object_size * options.dimentions(), 0);
		}

		void create_gl_buffer()
		{
			glGenBuffers(1, &id);
			copy_data_to_buffer();
			enable_vertex_buffer();
		}

		std::vector<BufferObjType> data;

		const buffer_opt options;
		const unsigned object_size = sizeof(BufferObjType);
		unsigned id;
	};
}

