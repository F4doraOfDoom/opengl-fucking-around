#pragma once

#include <string>
#include <cstdlib>
#include <GL\glew.h>

namespace j2d
{
	class __shader
	{
	public:
		virtual __shader& recompile(const std::string& code)
		{
			if (!self_initialized) throw j2d::shader_not_initialized();

			glDeleteShader(shader_id);

			source_code = code;
			create_shader();
			compile_shader();

			return *this;
		}

		virtual __shader& initialize()
		{
			if (self_initialized) throw j2d::shader_already_initialized();

			create_shader();
			self_initialized = true;

			return *this;
		}

		virtual bool compile_failed() const 
		{
			if (!self_initialized) throw j2d::shader_not_initialized();

			return compile_status == GL_FALSE;
		}

		virtual std::string get_error() const 
		{
			if (!self_initialized) throw j2d::shader_not_initialized();

			int length;
			glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &length);

			char* message = new char[length + 1];
			glGetShaderInfoLog(shader_id, length, &length, message);

			std::string out(message);
			delete[] message;

			return out;
		}

		virtual const __shader& use() const
		{
			if (!self_initialized) throw j2d::shader_not_initialized();

			glUseProgram(program_id);

			return *this;
		}

	protected:
		std::string source_code;
		unsigned shader_id;
		unsigned program_id;
		unsigned shader_type;
		int compile_status;
		bool self_initialized = true;

		__shader(GLuint type, const std::string& code) : shader_type(type), source_code(code)
		{
			if (__glewGetShaderiv == nullptr)
			{
				// OPEN_GL not initialized yet
				self_initialized = false;
				return;
			}

			create_shader();
		}

		inline  ~__shader()
		{
			glDeleteShader(shader_id);
		}

		void create_shader()
		{
			program_id = glCreateProgram();
			compile_shader();

			glAttachShader(program_id, shader_id);
			glLinkProgram(program_id);
			glValidateProgram(program_id);
		}

		void compile_shader()
		{
			shader_id = glCreateShader(shader_type);
			const char* src = source_code.c_str();
			glShaderSource(shader_id, 1, &src, nullptr);
			glCompileShader(shader_id);

			glGetShaderiv(shader_id, GL_COMPILE_STATUS, &compile_status);
		}
	};

	class vertex_shader : public __shader
	{
	public:
		vertex_shader(const std::string& code) : __shader(GL_VERTEX_SHADER, code) {}
	};

	class fragment_shader : public __shader
	{
	public:
		fragment_shader(const std::string& code) : __shader(GL_FRAGMENT_SHADER, code) {}
	};


}

