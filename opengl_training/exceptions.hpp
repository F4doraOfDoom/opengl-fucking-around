#pragma once
#include <string>

namespace j2d
{
	class __exception
	{
	public:
		virtual const char* what() const throw()
		{
			return reason;
		}

	protected:
		const char* reason;
	};

	class shader_failed_to_compile : __exception
	{
	public:
		shader_failed_to_compile(const char* reason) 
		{
			this->reason = reason;
		}
	};

	class shader_not_initialized : __exception
	{
	public:
		shader_not_initialized()
		{
			reason = "The shader has been constructed before OpenGL, "
				"so the object has skipped its object initialization phase. "
				"Please call __shader::initialize() if that is the case.";
		}
	};

	class shader_already_initialized : __exception
	{
	public:
		shader_already_initialized()
		{
			reason = "This shader has already been initialized, and so cannot "
				"be initialized again.";
		}
	};
}


