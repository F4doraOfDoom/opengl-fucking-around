#pragma once

#include <array>
#include "buffer_opt.hpp"
#include "compile_checks.hpp"

namespace j2d
{
	template <unsigned int N_Indices>
	class index_buffer
	{
	public:
		index_buffer(const std::array<unsigned int, N_Indices>&& indexes,
			const buffer_opt& opt = buffer_opt()) : i_data(indexes), options(opt) 
		{
			_init();
		}

		index_buffer(const std::array<unsigned int, N_Indices>& indexes,
			const buffer_opt& opt = buffer_opt()) : i_data(indexes), options(opt)
		{
			_init();
		}

		// TODO
		index_buffer(index_buffer&) = delete;
		void operator =(index_buffer&) = delete;

		void use_index_buffer()
		{
			glDrawElements(options.shape(), N_Indices, GL_UNSIGNED_INT, nullptr);
		}

	private:
		void _init()
		{
			glGenBuffers(1, &id);
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, id);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, N_Indices * sizeof(unsigned int), &i_data.at(0), options.usage());
		}

		std::array<unsigned int, N_Indices> i_data;
		const unsigned object_size = sizeof(unsigned int);
		const buffer_opt options = buffer_opt();
		unsigned int id;
	};
}